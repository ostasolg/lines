package lines;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Lines {
    private static Queue<int[]> lines;
    private static Queue<int[]> outputLines;

    public static void main(String[] args) {

        // creating a queue where all lines from the input will be stored
        lines = new PriorityQueue<>(new Comparator<int[]>() {
            // providing a comparing function for sorting which compares 2 lines
            @Override
            public int compare(int[] o1, int[] o2) {
                if (o1[0] == o2[0]) {
                    return o1[1] - o2[1];
                }
                return o1[0] - o2[0];
            }
        });
        // creating a queue where all lines for the output will be stored
        outputLines = new ArrayDeque<>();

        // reading an input from a user
        readInput();
        // solving the task and finding the needed lines
        countLine();
        // creating an output for the user
        returnOutput();
    }

    public static void countLine() {

        int lastNode = -1;

        // ending method if the queue is empty
        if (lines.size() == 0) {
            return;
        }
        // creating output if the queue contains only 1 line
        if (lines.size() == 1) {
            outputLines.add(lines.poll());
            return;
        }

        // getting a line from the prepared queue
        int[] currentLine = lines.poll();

        while (!lines.isEmpty()) {

            assert currentLine != null;

            // getting the next line from the prepared queue
            int[] nextLine = lines.poll();

            // comparing current and next lines and checking if the current line can be added to an output
            if (currentLine[0] == nextLine[0]) {
                // current line cannot be added to the output, therefore setting its value to the next line
                currentLine = nextLine;
                continue;
            }
            else if (currentLine[1] >= nextLine[1]) {
                // next line is inside the current line
                // continue comparing current line with another next line
                continue;
            }
            else if ((lastNode != -1) && (lastNode + 1 == nextLine[0])) {
                // no need to add current line
                currentLine = nextLine;
                // continue comparing current line with another next line
                continue;
            }
            // current line can be added to the output
            lastNode = currentLine[1];
            outputLines.add(currentLine);
            currentLine = nextLine;
        }
        if (!outputLines.contains(currentLine)) {
            outputLines.add(currentLine);
        }
    }

    public static void readInput() {

        String input = "";
        System.out.println("Write input lines:");

        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

            while (true) {

                // reading the input
                String line = br.readLine();

                // determining of the last input
                if (line.equals("/n") || line.equals("")) {
                    break;
                }
                // parsing the read input
                StringTokenizer tokenizer = new StringTokenizer(line);
                int firstIn = Integer.parseInt(tokenizer.nextToken());
                int secondIn = Integer.parseInt(tokenizer.nextToken());

                // adding information about a line to the queue
                lines.add(new int[]{firstIn, secondIn});
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void returnOutput() {
        System.out.println("Solution:");
        // printing all lines from the queue
        while (!outputLines.isEmpty()) {
            int[] line = outputLines.poll();
            System.out.println(line[0] + " " + line[1]);
        }
    }
}
